# overscroll-decor

## 简介
> 类似iOS风格的边缘滚动效果视图。
> 支持下列视图：RecyclerView, ListView, GridView, ViewPager, ScrollView, HorizontalScrollView, Any View - Text, Image...

![](screenshot/screenshot.gif)

## 下载安装
```shell
ohpm install @ohos/overscroll-decor
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)


## 主要接口列表


| 接口                          | 方法说明      | 
|-----------------------------|-----------|
| getHeight()                 | 获取组件高度    | 
| setHeight()                 | 设置组件高度    | 
| getWidth()                  | 获取组件宽度    | 
| setWidth()                  | 设置组件宽度    | 
| getMargin()                 | 获取组件边缘度   | 
| setMargin()                 | 设置组件边缘度   | 
| getOrientation()            | 获取组件方向    | 
| setOrientation()            | 设置组件方向    | 
| isScrollBar()               | 判断是否是滚动条  | 
| setScrollBar()              | 设置滚动条     | 
| isOverScrollBounceEffect()  | 是否有滚动反弹效果 | 
| setOverScrollBounceEffect() | 设置滚动反弹效果  | 
| isUpOverScroll()            | 是否向上滚动    | 
| setUpOverScroll()           | 设置向上滚动    | 
| getOffsetX()                | 获取离开X的值   | 
| setOffsetX()                | 设置离开X的值   | 
| getOffsetY()                | 获取离开Y的值   | 
| setOffsetY()                | 设置离开Y的值   | 
| getTextColor()              | 获取字体颜色    | 
| setTextColor()              | 设置字体颜色    | 
| getDragColorTop()           | 获取拖动顶部颜色  | 
| setDragColorTop()           | 设置拖动顶部颜色  | 




## 使用说明

### 提供滚动容器视图，使用方法类似，以GridViewDemo为例
### 1、初始化：实例化OverScrollDecor.Model 对象
 ```
private model: OverScrollDecor.Model = new OverScrollDecor.Model()
 ```
### 2、属性设置：通过Model类对象设置UI属性来自定义所需风格
 ```
private aboutToAppear() {
    this.model
      .setUpOverScroll(true)
      .setOrientation(OverScrollDecor.ORIENTATION.VERTICAL)
      .setOverScrollBounceEffect(true)
      .setScrollBar(true)
      .setWidth("100%")
      .setHeight("80%")
  }
 ```
### 3、子组件绘制：
 ```
@Builder SpecificChild() {
    Column({ space: 10 }) {
      ......
    }.width('100%')
  }
 ```
### 4、界面绘制：
 ```
build() {
  Stack({ alignContent: Alignment.TopStart }) {      
  ......
  OverScrollDecor({ model: this.model!!, child: () => { this.SpecificChild() } })
  ......
}
 ```
更多详细用法请参考开源库sample页面的实现

## 属性说明
1. 滚动组件高：默认px2vp(2340)
   `mHeight: number | string = px2vp(2340)`
2. 滚动组件宽：默认px2vp(lpx2px(720))
   `mWidth: number | string = px2vp(lpx2px(720))`
3. 滚动组件外边距：默认16
   `mMargin: number = 16`
4. 组件滚动方向：默认VERTICAL
   `mOrientation:  ORIENTATION = 0`
5. 滚动条设置：默认true
   `mScrollBar: boolean = true`
6. 边缘滚动效果设置：默认false
   `mOverScrollBounceEffect: boolean = false`
7. 滚动设置：默认true
   `mUpOverScroll: boolean = true`

## 约束与限制
在下述版本验证通过：

Deveco Studio NEXT Developer Beta3: 5.0.3.521,SDK:API12 (5.0.0.32)
DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## 目录结构
````
|---- overscroll-decor 
|     |---- entry  # 示例代码文件夹
|     |---- library  # overscroll库文件夹
|           |---- index.ets  # 对外接口
|           |---- src
|                  |---- main
|                        |---- ets
|                              |---- components
|                                    |---- OverScrollDecor.ets  #滚动功能实现
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/overscroll-decor/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/overscroll-decor/pulls) 共建。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/overscroll-decor/blob/master/LICENSE) ，请自由地享受和参与开源。